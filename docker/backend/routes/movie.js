const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
  const {movie_title, movie_release_date,movie_time, director_name } = request.body
console.log(request.body);
  const query = `
    INSERT INTO movie
      (movie_title, movie_release_date,movie_time, director_name)
    VALUES
      ('${movie_title}', ${movie_release_date}, '${movie_time}','${director_name}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/findMovieByName/:movie_title', (request, response) => {
  const query = `
    SELECT *
    FROM movie
    WHERE
    movie_title  = ${movie_title}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/update/:movie_id', (request, response) => {
  const { movie_id } = request.params
  const { movie_release_date,movie_time } = request.body

  const query = `
    UPDATE movie_release_date, movie_time
    SET
    movie_release_date = '${movie_release_date}', 
    movie_time = ${movie_time}, 
      
    WHERE
    movie_id = ${movie_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/removeById/:movie_id', (request, response) => {
  const { movie_id } = request.params

  const query = `
    DELETE FROM movie
    WHERE
    movie_id = ${movie_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router

